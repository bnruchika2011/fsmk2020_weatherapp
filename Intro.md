What is Vue?
How is that different from vanilla Js?
Comparing both.

Step_1 
    Start a vue instance. Congratulations, you created your first Vue.js App!!
    
Step_2
    Simple comparision between Vue and JS with an event handler.
    
Step_3
    Binding data to the html attributes dynamically. Use "v-bind".

Step_4
    How methods work in vue. How functions work?

Step_5
    Introduction to vue conditions like v-if, v-show and examples.

Step_6
    Looping in vue. Use of v-for. Example of looping around a list of elements.
    
Step_7
    Vue components.